import cv2
import time
import os
import numpy as np
from PIL import Image, ImageDraw, ImageFilter
import json
from pathlib import Path

import genwidth
from pixutils import pix_dist


def convert_from_cv2_to_image(img: np.ndarray) -> Image:
    return Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


def convert_from_image_to_cv2(img: Image) -> np.ndarray:
    # return np.asarray(img)
    return cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)


# Setup data
Path("data").mkdir(exist_ok=True)
bbox = [(482//2, 590//2), (719//2, 912//2)]
BBOX_EXT = 20
bbox_large = [(bbox[0][0] - BBOX_EXT, bbox[0][1] - BBOX_EXT), (bbox[1][0] + BBOX_EXT, bbox[1][1] + BBOX_EXT)]
types = ["ACE", "TWO", "THREE", "FOUR", "FIVE",
         "SIX", "SEVEN", "EIGHT", "NINE", "TEN",
         "JACK", "QUEEN", "KING"]
current_type = 10  # 13
standby = True

# define a video capture object
vid = cv2.VideoCapture(0)
vid.set(cv2.CAP_PROP_FRAME_WIDTH, genwidth.IMG_WIDTH)
vid.set(cv2.CAP_PROP_FRAME_HEIGHT, genwidth.IMG_HEIGHT)
vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
tran = 0
start = time.time()
print(">>> ON STANDBY, PRESS 'n' TO BEGIN GENERATING DATA: " + str(types[current_type]))


while True:
    tran += 1
    ret, frame = vid.read()

    pil_image = convert_from_cv2_to_image(frame)
    clean_img = convert_from_cv2_to_image(frame)

    imd = ImageDraw.Draw(pil_image)
    imd.rectangle(bbox, outline='red')
    imd.rectangle(bbox_large, outline='cyan')
    marked_frame = convert_from_image_to_cv2(pil_image)

    small = cv2.resize(marked_frame, (0, 0), fx=2, fy=2)
    cv2.imshow('frame', small)

    if cv2.waitKey(1) & 0xFF == ord('n'):
        fname = str(round(time.time() * 1000))
        data_dir = "datagen/cards/%s/%s.png" % (types[current_type], fname)
        Path(os.path.dirname(data_dir)).mkdir(parents=True, exist_ok=True)

        clean_img = clean_img.crop(
            (bbox_large[0][0], bbox_large[0][1], bbox_large[1][0], bbox_large[1][1])
        )

        clean_img = clean_img.convert("RGBA")
        pixdata = clean_img.load()
        width, height = clean_img.size

        for y in range(height):
            for x in range(width):
                if pix_dist(pixdata[x, y], (111, 43, 13)) < 40 \
                        or pix_dist(pixdata[x, y], (75, 22, 1)) < 32:
                    if x < 31 or x > 31+99 or 30 > y or y > 30+144:
                        pixdata[x, y] = (255, 255, 255, 0)

        img_blur = clean_img.copy()
        img_blur = img_blur.filter(ImageFilter.GaussianBlur(radius=1))
        img_blur.paste(clean_img, (0, 0), clean_img)

        img_blur.save(data_dir, "PNG")

        # cv2.imwrite(data_dir, frame)
        time.sleep(1)

# TODO Commit or delete

vid.release()
cv2.destroyAllWindows()

end = time.time()

elapsed = end - start
print("Elapsed: %d, Frames: %d, FPS: %s" % (elapsed, tran, tran / elapsed))
