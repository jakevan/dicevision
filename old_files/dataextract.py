import math
from math import sqrt

from PIL import Image, ImageDraw
import numpy as np

import globalwidth
import hdr


def gethex(s):
    return tuple(int(s[i:i + 2], 16) for i in (0, 2, 4))

img_width = globalwidth.IMG_WIDTH
img_height = globalwidth.IMG_HEIGHT
R_p = gethex("3BADF4")
def get_coords(im):


    min_dist_r = 1000000

    # r_data = (0, 0)
    r_data = []

    pix = im.load()
    for x in range(10, img_width-10):
        for y in range(10, img_height-10):
            rgb = pix[x, y]
            dist_r_r = abs(R_p[0] - rgb[0])
            dist_r_g = abs(R_p[1] - rgb[1])
            dist_r_b = abs(R_p[2] - rgb[2])
            dist_r = dist_r_r + dist_r_g + dist_r_b
            if dist_r < 140:
                r_data.append((x, y))
            # if min_dist_r > dist_r:
            #     min_dist_r = dist_r
            #     r_data = (x, y)

    imd = ImageDraw.Draw(im)
    r = 50
    center_x = 0
    center_y = 0
    if len(r_data) > 0:
        for id in r_data:
            center_x += id[0]
            center_y += id[1]
            imd.point(id, fill='red')
        center_x //= len(r_data)
        center_y //= len(r_data)
        r = 50
        imd.ellipse((center_x - r, center_y - r, center_x + r, center_y + r), outline='blue', width=4)

    im = hdr.make_hdr(im)
    return im, center_x, center_y

def clean(im):
    imd = ImageDraw.Draw(im)
    pix = im.load()
    for x in range(10, img_width-10):
        for y in range(10, img_height-10):
            rgb = pix[x, y]
            dist_r_r = abs(R_p[0] - rgb[0])
            dist_r_g = abs(R_p[1] - rgb[1])
            dist_r_b = abs(R_p[2] - rgb[2])
            dist_r = dist_r_r + dist_r_g + dist_r_b
            if dist_r < 110:
                imd.point((x, y), fill='white')
    return im
