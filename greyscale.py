import os
import pathlib
import shutil
from PIL import Image, ImageDraw, ImageOps
import genwidth

data_dir = pathlib.Path("gdata")

files = list(data_dir.glob('*/*.png'))
for f in files:
    pathlib.Path(os.path.dirname(f)).mkdir(parents=True, exist_ok=True)
    img = Image.open(str(f))
    img = img.convert("L")
    img = img.resize((genwidth.IMG_WIDTH, genwidth.IMG_HEIGHT))
    img.save(f)



