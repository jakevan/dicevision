import math
import os


def pix_dist(inp: tuple, alpha_color) -> int:
    r_dist = abs(inp[0] - alpha_color[0])
    g_dist = abs(inp[1] - alpha_color[1])
    b_dist = abs(inp[2] - alpha_color[2])
    return r_dist + g_dist + b_dist


types = []
for f in os.listdir("data"):
    types.append(f)
print(types)

def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy