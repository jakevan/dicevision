import math
import random
import sys
import time
from pathlib import Path
import threading

import PIL.Image
from PIL import Image, ImageDraw, ImageChops, ImageFilter, ImageOps
import cv2 as cv
import numpy as np

import genwidth
import pixutils
from pixutils import pix_dist
from matplotlib import pyplot as plt
import os, random
import itertools


def random_path_from_dir(directory):
    return directory + '/' + random.choice(os.listdir(directory))


def convert_from_cv2_to_image(img: np.ndarray) -> Image:
    return Image.fromarray(cv.cvtColor(img, cv.COLOR_BGR2RGB))


def convert_from_image_to_cv2(img: Image) -> np.ndarray:
    # return np.asarray(img)
    return cv.cvtColor(np.array(img), cv.COLOR_RGB2BGR)


ty = pixutils.types
progress = itertools.count()

PER_ITER = 2000  # 1000


def run_for(ran):
    t = ty[ran]
    print("Running %d of %d" % (ran, len(ty)))
    if t == "NO_DICE":
        return
    for to_run in range(PER_ITER):
        prg = next(progress)

        sys.stdout.write('\r')
        sys.stdout.write(str(prg) + "/" + str(PER_ITER * len(ty)))
        sys.stdout.flush()

        # print(str(prg) + "/" + str(PER_ITER*len(ty)))
        img = Image.open(random_path_from_dir('data/' + t))

        # Random Rotation
        angle = random.randint(0, 360)
        clean_img = img.rotate(angle, expand=False)

        # Random zoom
        # zoom_scale = random.uniform(0.95, 0.)
        # zx = int(genwidth.IMG_WIDTH * zoom_scale)
        # zy = int(genwidth.IMG_HEIGHT * zoom_scale)
        # clean_img = clean_img.resize((zx, zy), resample=PIL.Image.BICUBIC)
        # clean_img = ImageOps.expand(clean_img, ((genwidth.IMG_WIDTH - zx) // 2, (genwidth.IMG_HEIGHT - zy) // 2), fill='black')
        # dice_size = 220 * zoom_scale

        # Random Translation
        rx = random.randint(-235, 235)  # 270
        ry = random.randint(-135, 135)  # 175
        clean_img = clean_img.transform(img.size, Image.AFFINE, (1, 0, rx, 0, 1, ry))

        origin = (320 - rx, 240 - ry)

        # imd = ImageDraw.Draw(clean_img)
        # size = 30
        # imd.point(origin, fill="white")
        # imd.ellipse((origin[0] - size, origin[1] - size, origin[0] + size, origin[1] + size), outline='red')

        clean_img = clean_img.convert("RGBA")
        pixdata = clean_img.load()
        width, height = clean_img.size

        for y in range(height):
            for x in range(width):
                dat = pixdata[x, y]
                if dat[0] == dat[1] == dat[2] == 0:
                    pixdata[x, y] = (0, 0, 0, 0)

        paper = Image.open("paper.png")
        paper.paste(clean_img, (0,0), clean_img)
        paper = paper.convert("RGB")
        # paper.show()

        fname = str(round(time.time() * 1000))
        path = "datagen/%s/_%d_%d_%s.png" % (t, origin[0], origin[1], fname)

        Path(os.path.dirname(path)).mkdir(parents=True, exist_ok=True)
        # clean_img.show()
        paper.save(path)

class myThread(threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter

    def run(self):
        run_for(self.counter)


rx = 0
for t in ty:
    thread = myThread(rx, "Thread-" + str(rx), rx)
    thread.start()
    rx += 1
