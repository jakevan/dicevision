# CardVision
Trained with only 13 images and a lot of data augmentation!

![demo](card.mp4)
# Requirements:
 - tensorflow
 - opencv-python
 - pillow
 - matplotlib
 - tqdm

 - curl https://repo.anaconda.com/archive/Anaconda3-2022.10-Linux-x86_64.sh --output anaconda.sh
 - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/