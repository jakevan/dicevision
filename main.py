# Credit: https://www.tensorflow.org/tutorials/images/cnn
# https://www.tensorflow.org/tutorials/images/classification
# Used the tutorials net structure (kinda) acuracy graphs, and a few other things


import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from PIL import Image, ImageDraw

from tensorflow import keras
from tensorflow.keras import datasets, layers, models
from tensorflow.keras.models import Sequential

import pathlib

import genwidth
import hdr

import main_pos

data_dir = pathlib.Path("data")

image_count = len(list(data_dir.glob('*/*.png')))

print(image_count)

batch_size = 64
img_height = genwidth.IMG_HEIGHT
img_width = genwidth.IMG_WIDTH

list_ds = tf.data.Dataset.list_files(str(data_dir / '*/*'), shuffle=True)

REPEAT = 200
list_ds = list_ds.repeat(REPEAT)
list_ds = list_ds.shuffle(image_count * REPEAT, reshuffle_each_iteration=True)


for f in list_ds.take(5):
    print(f.numpy())

class_names = np.array(sorted([item.name for item in data_dir.glob('*')]))
print(class_names)

# val_size = int(image_count * REPEAT * 0.2)
train_ds = list_ds #list_ds.skip(val_size)
val_ds = list_ds.take(image_count)


def get_label(file_path):
    parts = tf.strings.split(file_path, os.path.sep)
    one_hot = parts[-2] == class_names
    return tf.argmax(one_hot)


def get_xy(file_path):
    parts = tf.strings.split(file_path, '_')
    return int(parts[-2]), int(parts[-1])


def decode_img(img):
    img = tf.io.decode_jpeg(img, channels=3)
    return tf.image.resize(img, [img_height, img_width])  # resize probs not needed


def process_path(file_path):
    label = get_label(file_path)
    img = tf.io.read_file(file_path)
    img = decode_img(img)
    # xy = get_xy(file_path)
    return img, label


AUTOTUNE = tf.data.AUTOTUNE
train_ds = train_ds.map(process_path, num_parallel_calls=AUTOTUNE)
val_ds = val_ds.map(process_path, num_parallel_calls=AUTOTUNE)


def configure_for_performance(ds):
    ds = ds.shuffle(buffer_size=1000)
    ds = ds.batch(batch_size)
    ds = ds.prefetch(buffer_size=AUTOTUNE)
    # ds = ds.cache()
    return ds


train_ds = configure_for_performance(train_ds)
val_ds = configure_for_performance(val_ds)

# train_ds = train_ds.cache()

num_classes = len(class_names)

data_augmentation = keras.Sequential(
    [
        # layers.RandomFlip("horizontal_and_vertical",),
        layers.RandomRotation(1, input_shape=(img_height,
                                       img_width,
                                       3)),

        # Lighting layers
        layers.RandomBrightness(0.08),
        layers.RandomContrast(0.08),

        # layers.RandomHeight(1), #these 2 do not work
        # layers.RandomWidth(0.05),
        layers.RandomZoom(0.05),
        # Height = 170px to spare, 480/170
        # Actually works fine for 240p since the ratios are the same.
        layers.RandomTranslation(170.0 / 480.0, 270.0 / 640.0, fill_mode="nearest")
    ]
)

# Show some augmented data (from TF tutorial) (throws about 10000000 warnings)

# plt.figure(figsize=(10, 10))
# for images, _ in train_ds.take(1):
#     for i in range(25):
#         augmented_images = data_augmentation(images)
#         ax = plt.subplot(5, 5, i + 1)
#         plt.imshow(augmented_images[0].numpy().astype("uint8"))
#         plt.axis("off")
# plt.show()

mirrored_strategy = tf.distribute.MirroredStrategy()
savedir = "save/m1.ckpt"
# with mirrored_strategy.scope():
if True:
    # https://github.com/nell-byler/dice_detection/blob/master/0_dice_classification.ipynb
    # This was the best model I found, only 2 conv layers really helps. And big kernel
    # And did a lil' augmentation
    model = Sequential([
        data_augmentation,
        layers.Rescaling(1. / 255),
        layers.Conv2D(16, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        # layers.Dropout(0.01), # Because our data augmentation is so epic dropout doesnt really do much
        layers.Flatten(),
        # layers.Dense(128, activation='relu'), # do NOT add more layers, just destroys the accuracy
        layers.Dense(128, activation='relu'),
        layers.Dense(num_classes)
    ])

    model.compile(optimizer='adam',
                  # TF tutorial tells me Categorical Crossentropy is good
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

    model.summary()
    try:
        model.load_weights(savedir)
    except:
        print("could not load weights")
    if __name__ == '__main__':

        epochs = 100  # 10 actually works
        history = model.fit(
            train_ds,
            validation_data=val_ds,
            epochs=epochs,
            callbacks=[
                tf.keras.callbacks.ModelCheckpoint(filepath=savedir,
                                                   save_weights_only=True,
                                                   verbose=1,
                                                   save_freq=207)]
        )
        # model.save_weights("save/model.h5")

        acc = history.history['accuracy']
        val_acc = history.history['val_accuracy']

        loss = history.history['loss']
        val_loss = history.history['val_loss']

        epochs_range = range(epochs)

        plt.figure(figsize=(8, 8))
        plt.subplot(1, 2, 1)
        plt.plot(epochs_range, acc, label='Training Accuracy')
        plt.plot(epochs_range, val_acc, label='Validation Accuracy')
        plt.legend(loc='lower right')
        plt.title('Training and Validation Accuracy')

        plt.subplot(1, 2, 2)
        plt.plot(epochs_range, loss, label='Training Loss')
        plt.plot(epochs_range, val_loss, label='Validation Loss')
        plt.legend(loc='upper right')
        plt.title('Training and Validation Loss')
        # plt.show()


    def convert_from_cv2_to_image(img: np.ndarray) -> Image:
        return Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


    def convert_from_image_to_cv2(img: Image) -> np.ndarray:
        # return np.asarray(img)
        return cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)


    def half_scale(cv_img):
        width = int(cv_img.shape[1] // 2)
        height = int(cv_img.shape[0] // 2)
        dim = (width, height)

        return cv2.resize(cv_img, dim, interpolation=cv2.INTER_AREA)


    # exit(1)
    vid = cv2.VideoCapture(0)
    vid.set(cv2.CAP_PROP_FRAME_WIDTH, genwidth.IMG_WIDTH)
    vid.set(cv2.CAP_PROP_FRAME_HEIGHT, genwidth.IMG_HEIGHT)
    vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
    vid.set(cv2.CAP_PROP_FOCUS, 7)
    vid.set(cv2.CAP_PROP_EXPOSURE, 100)
    focus = 5
    while True:
        ret, frame = vid.read()

        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # halfscale = half_scale(frame)
        # hs_tensor = tf.convert_to_tensor(halfscale, dtype=tf.float32)
        # half_array = tf.expand_dims(hs_tensor, 0)

        rgb_tensor = tf.convert_to_tensor(rgb, dtype=tf.float32)
        img_array = tf.expand_dims(rgb_tensor, 0)

        predictions = model.predict(img_array, steps=1)
        score = tf.nn.softmax(predictions[0])

        pos_predictions = main_pos.get_pos_model().predict(img_array, steps=1)

        print(
            "This image most likely belongs to {} with a {:.2f} percent confidence."
            .format(class_names[np.argmax(score)], 100 * np.max(score))
        )
        img = convert_from_cv2_to_image(frame)
        hdr.make_hdr(img)
        imd = ImageDraw.Draw(img)
        imd.text((10, 10), "DICE: " + str(class_names[np.argmax(score)]), fill="red", stroke_width=2, )

        p0 = pos_predictions[0]

        size = 30
        origin = (p0[0], p0[1])
        imd.point(origin, fill="white")
        imd.ellipse((origin[0] - size, origin[1] - size, origin[0] + size, origin[1] + size), outline='red')

        cv2img = convert_from_image_to_cv2(img)
        cv2img = cv2.resize(cv2img, (0, 0), fx=2, fy=2)
        cv2.imshow('frame', cv2img)
        k = cv2.waitKey(1) & 0xFF
        if k == ord('q'):
            break
        elif k == ord('a'):
            focus -= 5
        elif k == ord('s'):
            focus += 5
            print(focus)
        vid.set(cv2.CAP_PROP_FOCUS, focus)

    vid.release()
    cv2.destroyAllWindows()
